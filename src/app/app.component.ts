import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';

import firebase from 'firebase';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    
    firebase.initializeApp({
    apiKey: "AIzaSyAswF8BIA2sBa2EyYlDNDDMTsuAf1nUut4",
    authDomain: "fir-chat-8c0ce.firebaseapp.com",
    databaseURL: "https://fir-chat-8c0ce.firebaseio.com",
    projectId: "fir-chat-8c0ce",
    storageBucket: "fir-chat-8c0ce.appspot.com",
    messagingSenderId: "705640282033"
  
    });

   

    const unsubscribe = firebase.auth().onAuthStateChanged((user) => {
      if (!user) {
        this.rootPage = 'login';
        unsubscribe();
      } else { 
        this.rootPage = HomePage;
        unsubscribe();
      }
    });

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}