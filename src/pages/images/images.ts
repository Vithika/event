import { Component,NgZone } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import {  File} from '@ionic-native/file';
import{FileChooser}from'@ionic-native/file-chooser';
import{FilePath}from'@ionic-native/file-path';

import firebase from 'firebase';
/**
 * Generated class for the ImagesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-images',
  templateUrl: 'images.html',
})


export class ImagesPage {
    nativepath: any;
  firestore = firebase.storage();
  imgsource: any;

  constructor(public navCtrl: NavController, public zone:NgZone,    public navParams: NavParams,public FileChooser:FileChooser,public FilePath:FilePath,public File:File) {
    
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ImagesPage');
  }

   store() {
   this. FileChooser.open().then((url) => {
      (<any>window).FilePath.resolveNativePath(url, (result) => {
        this.nativepath = result;
        this.uploadimage();
      }
      )
    })
  }

  uploadimage() {
    (<any>window).resolveLocalFileSystemURL(this.nativepath, (res) => {
      res.file((resFile) => {
        var reader = new FileReader();
        reader.readAsArrayBuffer(resFile);
        reader.onloadend = (evt: any) => {
          var imgBlob = new Blob([evt.target.result], { type: 'image/jpeg' });
          var imageStore = this.firestore.ref().child('image');
          imageStore.put(imgBlob).then((res) => {
            alert('Upload Success');
          }).catch((err) => {
            alert('Upload Failed' + err);
          })
        }
      })
    })
  }

  display() {
    this.firestore.ref().child('image').getDownloadURL().then((url) => {
      this.zone.run(() => {
        this.imgsource = url;
       })
    })
  }

}




 

